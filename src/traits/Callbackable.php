<?php
/**
 * Callbackable.php
 * /src/traits
 * 
 * @author Jesús Rojas <jrojash@jys.pe>
 * @copyright 2023, J&S Perú <https://jys.pe>
 * @created 2023-09-11 22:22:45
 * @version 20230913171442 (Rev. 67)
 * @filesource
 */

/**
 * La clase asociada puede alojar múltiples hooks que se pueden ejecutar en cualquier momento
 */
trait Callbackable
{
    /** $_static_callbacks */
    public static $_static_callbacks = [];

    /** $_callbacks */
    protected $_callbacks = [];

    /**
     * Agrega una función a ejecutar de manera global en cualquier instancia que se cree a nivel de clase
     * @param string $key
     * @param callable $callback
     * @return void
     */
    public static function addGlobalCallback(string $key, callable $callback): void
    {
        $callbacks =& static::$_static_callbacks;
        isset($callbacks[$key]) or $callbacks[$key] = [];

        $callbacks[$key][] = $callback;
    }

    /**
     * Agrega una función a ejecutar unicamente a la instancia actual
     * @param string $key
     * @param callable $callback
     * @return mixed
     */
    public function addInstanceCallback(string $key, callable $callback): static
    {
        $callbacks =& $this->_callbacks;
        isset($callbacks[$key]) or $callbacks[$key] = [];

        $callbacks[$key][] = $callback;

        return $this;
    }

    /**
     * Función que busca los hooks a ejecutar y retorna la respuesta tras la ejecución
     *
     * > Se ejecutan primero los callacks a nvel de clase y después a nivel de instancia
     *
     * @param string $key
     * @param mixed $return
     * @param array $params
     * @return mixed
     */
    public function execCallbacks(string $key, mixed $return = null, ...$params): mixed
    {
        array_unshift($params, $return);
        $params[] = $this;
        $params[] = $key;

        $callbacks = static::$_static_callbacks;
        $callbacks = $callbacks[$key] ?? [];
        $callbacks = (array) $callbacks;
        foreach ($callbacks as $callback) {
            try {
                $temp = call_user_func_array($callback, $params);
                $params[0] = $temp;
            }
            catch (Exception $e) {
            }
        }

        $callbacks = $this->_callbacks;
        $callbacks = $callbacks[$key] ?? [];
        $callbacks = (array) $callbacks;
        foreach ($callbacks as $callback) {
            try {
                $temp = call_user_func_array($callback, $params);
                $params[0] = $temp;
            }
            catch (Exception $e) {
            }
        }

        return $params[0];
    }
}