<?php
/**
 * Instanceable.php
 * /src/traits
 * 
 * @author Jesús Rojas <jrojash@jys.pe>
 * @copyright 2023, J&S Perú <https://jys.pe>
 * @created 2023-09-11 19:47:28
 * @version 20230911195023 (Rev. 57)
 * @filesource
 */

/**
 * La clase asociada solo puede ser instanciada una sola vez
 * Una vez instanciada se llama a la función `_init` de la clase
 */
trait Instanceable
{
    /**
     * Devuelve la única instancia generada
     * @return	Instanceable
     */
    public static function instance(): static
    {
        static $_instance;

        if (!isset($_instance)) {
            $_instance = new static();
            $_instance->_init();
        }

        return $_instance;
    }

    /**
     * El constructor de la clase ahora es protegido y solo puede ser llamado por la misma clase
     * de esa manera se asegura que solo se pueda instanciar con el método estático `instance`
     *
     * > Utilizar: class:instance()
     */
    protected function __construct()
    {
    }

    /**
     * Función que se ejecutará inmediatamente tras la generación de la instancia.
     */
    protected function _init(): void
    {
    }
}