<?php
/**
 * Arrayable.php
 * /src/traits
 * 
 * @author Jesús Rojas <jrojash@jys.pe>
 * @copyright 2023, J&S Perú <https://jys.pe>
 * @created 2023-09-11 22:10:03
 * @version 20230913171510 (Rev. 33)
 * @filesource
 */

/**
 * La clase asociada puede simular ser una variable array y a la vez poder ejecutar funciones de clase
 */
trait Arrayable
{
    use Callbackable;

    /**
     * Permite retornar la primera variable encontrada dentor de los datos
     *
     * > Retorna NULL en caso de no encontrar algún elemento
     * 
     * @return mixed
     */
    public function first(): mixed
    {
        if (isset($this[0]))
            return $this[0];

        $return = NULL;
        foreach ($this as $v) {
            $return = $v;
            break;
        }

        return $return;
    }

    /**
     * Permite limpiar todos los elementos que contiene la clase
     * @return Arrayable
     */
    public function clear(): static
    {
        $keys = array_keys((array) $this);

        while (count($keys) > 0) {
            $key = array_shift($keys);
            $this->offsetUnset($key);
        }

        return $this;
    }

    /**
     * Función que permite detectar un código 
     * @param string $index
     * @return string
     */
    protected function _detect_index(string $index): string
    {
        if ($this->__isset($index))
            return $index;

        $indices = [$index];
        $temp = $index . 's' and $indices[] = $temp;
        $temp = $index . 'es' and $indices[] = $temp;
        $temp = preg_replace('/s$/i', '', $index) and $indices[] = $temp;
        $temp = preg_replace('/es$/i', '', $index) and $indices[] = $temp;
        $indices = array_unique($indices);

        foreach ($indices as $indice)
            if ($this->__isset($indice))
                return $index;

        return $index;
    }

    /**
     * Ejecuta una función predefinida
     * @param string $name
     * @param array $args
     * @return mixed
     */
    public function __call(string $name, array $args): mixed
    {
        if (preg_match('#^set_(.+)#', $name)) {
            $index = preg_replace('#^set_#', '', $name);
            $index = $this->_detect_index($index);
            $valor = array_shift($args);
            $this->__set($index, $valor);
            return $this;
        }

        if (preg_match('#^get_(.+)#', $name)) {
            $index = preg_replace('#^get_#', '', $name);
            $index = $this->_detect_index($index);
            return $this->__get($index);
        }

        if (preg_match('#^(add|agregar|push)_(.+)#', $name)) {
            $index = preg_replace('#^(add|agregar|push)_#', '', $name);
            $index = $this->_detect_index($index);

            $val = array_shift($args);
            $key = array_shift($args);

            $actual = $this->__isset($index) ? $this->__get($index) : [];
            $actual = (array) $actual;

            if (is_null($key))
                $actual[] = $val;
            else
                $actual[$key] = $val;

            $this->__set($index, $actual);
            return $this;
        }

        if (preg_match('#^(array_shift|shift)_(.+)#', $name)) {
            $index = preg_replace('#^(array_shift|shift)_#', '', $name);
            $index = $this->_detect_index($index);

            $actual = $this->__isset($index) ? $this->__get($index) : [];
            $actual = (array) $actual;
            $return = array_shift($actual);
            $this->__set($index, $actual);

            return $return;
        }

        if (preg_match('#^(array_pop|pop)_(.+)#', $name)) {
            $index = preg_replace('#^(array_pop|pop)_#', '', $name);
            $index = $this->_detect_index($index);

            $actual = $this->__isset($index) ? $this->__get($index) : [];
            $actual = (array) $actual;
            $return = array_pop($actual);
            $this->__set($index, $actual);

            return $return;
        }

        if (preg_match('#^(array_diff|diff)_(.+)#', $name)) {
            $index = preg_replace('#^(array_diff|diff)_#', '', $name);
            $index = $this->_detect_index($index);

            $actual = $this->__isset($index) ? $this->__get($index) : [];
            $actual = (array) $actual;
            $args = (array) $args;
            array_unshift($args, $actual);

            return call_user_func_array('(array_diff|diff)', $args);
        }

        trigger_error('Función requerida no existe `' . get_called_class() . '::' . $name . '()`', E_USER_WARNING);
        return $this;
    }

    /**
     * Permite a la clase ser una función
     * @return mixed
     */
    public function __invoke(): mixed
    {
        $_args = func_get_args();
        $index = array_shift($_args);
        $val = array_shift($_args);

        if (is_null($index))
            return $this->__toArray();

        if (is_null($val))
            return $this->__get($index);

        $this->__set($index, $val);
        return $this;
    }

    /**
     * Permite a la clase convertirse en una cadena string
     * @return string
     */
    public function __toString(): string
    {
        return json_encode((array) $this);
    }

    /**
     * Permite a la clase retornarse como array al llamarse en modo debug
     * @return array
     */
    public function __debugInfo(): array
    {
        return $this->__toArray();
    }

    ################################################
    ## Magic Functions / Array Access             ##
    ################################################

    /**
     * @param mixed $index
     * @return bool
     */
    public function offsetExists(mixed $index): bool
    {
        $this->execCallbacks('before_exists', $index);

        $return = parent::offsetExists($index);

        $return = $this->execCallbacks('exists', $return, $index);
        $return = $this->execCallbacks(__FUNCTION__, $return, $index);

        return $return;
    }

    /**
     * @param mixed $index
     * @return mixed
     */
    public function offsetGet(mixed $index): mixed
    {
        if ($method = '_before_get_' . $index and method_exists($this, $method))
            $this->$method($index);

        if ($method = '_before_get' and method_exists($this, $method))
            $this->$method($index);

        $this->execCallbacks('before_get', $index);
        $this->execCallbacks('before_get_' . $index);

        $return = parent::offsetGet($index);

        $return = $this->execCallbacks('get', $return, $index);
        $return = $this->execCallbacks('get_' . $index, $return);
        $return = $this->execCallbacks(__FUNCTION__, $return, $index);

        if ($method = '_after_get_' . $index and method_exists($this, $method))
            $return = $this->$method($return, $index);

        if ($method = '_after_get' and method_exists($this, $method))
            $return = $this->$method($return, $index);

        return $return;
    }
    /**
     * @param mixed $index
     * @param mixed $newval
     * @return void
     */
    public function offsetSet(mixed $index, mixed $newval): void
    {
        if ($method = '_before_set_' . $index and method_exists($this, $method))
            $newval = $this->$method($newval, $index);

        if ($method = '_before_set' and method_exists($this, $method))
            $newval = $this->$method($newval, $index);

        $newval = $this->execCallbacks('before_set', $newval, $index);
        $newval = $this->execCallbacks('before_set_' . $index, $newval);

        parent::offsetSet($index, $newval);

        $this->execCallbacks('set', $newval, $index);
        $this->execCallbacks('set_' . $index, $newval);
        $this->execCallbacks(__FUNCTION__, $newval, $index);

        if ($method = '_after_set_' . $index and method_exists($this, $method))
            $this->$method($newval, $index);

        if ($method = '_after_set' and method_exists($this, $method))
            $this->$method($newval, $index);

        return;
    }

    /**
     * @param mixed $index
     * @return void
     */
    public function offsetUnset(mixed $index): void
    {
        $this->execCallbacks('before_unset', $index);
        $this->execCallbacks('before_unset_' . $index);

        parent::offsetUnset($index);

        $this->execCallbacks('unset', $index);
        $this->execCallbacks('unset_' . $index);
        $this->execCallbacks(__FUNCTION__, $index);

        return;
    }

    ################################################
    ## Magic Functions / Object Access            ##
    ################################################

    /**
     * @param string $index
     * @return bool
     */
    public function __isset(string $index): bool
    {
        $return = $this->offsetExists($index);
        $this->execCallbacks(__FUNCTION__, $return, $index);
        return $return;
    }

    /**
     * @param string $index
     * @return mixed
     */
    public function __get(string $index): mixed
    {
        $return = $this->offsetGet($index);
        $this->execCallbacks(__FUNCTION__, $return, $index);
        return $return;
    }

    /**
     * @param string $index
     * @param mixed $newval
     * @return void
     */
    public function __set(string $index, mixed $newval): void
    {
        $this->offsetSet($index, $newval);
        $this->execCallbacks(__FUNCTION__, $newval, $index);
        return;
    }

    /**
     * @param string $index
     * @return void
     */
    public function __unset(string $index): void
    {
        $this->offsetUnset($index);
        $this->execCallbacks(__FUNCTION__, $index);
        return;
    }

    /**
     * @return array
     */
    public function __toArray(): array
    {
        $this->execCallbacks('before_toarray');

        $return = (array) $this;

        $return = $this->execCallbacks('toarray', $return);
        $return = $this->execCallbacks(__FUNCTION__, $return);

        return $return;
    }
}