<?php
/**
 * with.php
 * /src/functions
 * 
 * @author Jesús Rojas <jrojash@jys.pe>
 * @copyright 2023, J&S Perú <https://jys.pe>
 * @created 2023-09-05 14:48:14
 * @version 20230905150438 (Rev. 2)
 * @filesource
 */

if (!function_exists('with')) {
    /**
     * Permite interactuar con los parametros enviados
     * Al enviarse alguna función, todos los datos previos son enviados como parametros a aquella función
     * La función enviada como parámetro puede retornar nuevos datos que reemplazarán a los argumentos anteriores
     * 
     * > El último parametro debe ser una función
     * 
     * @param mixed $param
     * @param mixed $param2
     * @param mixed $param3
     * @param mixed $param_n
     * @param callable $func
     * @return mixed
     */
    function with(mixed ...$params): mixed
    {
        $args = [];
        $res = null;

        foreach ($params as $param) {
            if (is_callable($param)) {
                $res = call_user_func_array($param, $args);

                is_null($res) or $args = (array) $res;
                continue;
            }

            $args[] = $param;
        }

        return $res;
    }
}