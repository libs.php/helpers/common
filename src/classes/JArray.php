<?php
/**
 * JArray.php
 * /src/classes
 * 
 * @author Jesús Rojas <jrojash@jys.pe>
 * @copyright 2023, J&S Perú <https://jys.pe>
 * @created 2023-09-11 22:31:20
 * @version 20230911223801 (Rev. 692)
 * @filesource
 */

class JArray extends ArrayObject
{
    use Arrayable;
    use Callbackable;

    /**
     * @param array|JArray|Arrayable|ArrayObject|ArrayAccess $data
     * @param array|JArray|Arrayable|ArrayObject|ArrayAccess $callbacks
     */
    public function __construct(array|JArray|Arrayable|ArrayObject|ArrayAccess $data = [], array|JArray|Arrayable|ArrayObject|ArrayAccess $callbacks = [])
    {
        parent::__construct($data);
        $this->_callbacks = $callbacks;
    }

    /**
     * Contexto a manejar el objeto array
     * 
     * Posibles valores recomendados:
     * - edit
     * - view
     * - export
     * 
     * Dependiendo del contexto se puede transformar la data requerida del array
     * 
     * @var string
     */
    protected $_default_context = 'edit';

    /**
     * Permite establecer el contexto enviado en el parametro o recibir el contexto en el que se encuentra si no se envía parámetro alguno
     * @param mixed $context
     * @return JArray|string Retorna el contexto (string) en caso no se envíe parámetro alguno
     */
    public function defaultContext(?string $context = null): JArray|string
    {
        if (!is_null($context)) {
            $this->_default_context = $context;
            return $this;
        }

        return $this->_default_context;
    }

    /**
     * Reemplaza la función por defecto para poder utilizar el contexto al momento de obtener la data y ejecutar funciones que puedan transformarlo
     * @param mixed $index
     * @return mixed
     */
    public function offsetGet(mixed $index): mixed
    {
        $context = $this->defaultContext();

        if ($method = '_before_get_' . $index and method_exists($this, $method))
            $this->$method($index, $context);

        $this->execCallbacks('before_get', $index, $context);
        $this->execCallbacks('before_get_' . $index, $context);
        $this->execCallbacks('before_get_' . $index . '_' . $context);

        $return = parent::offsetGet($index);

        $return = $this->execCallbacks('get', $return, $index, $context);
        $return = $this->execCallbacks('get_' . $index, $return, $context);
        $return = $this->execCallbacks('get_' . $index . '_' . $context, $return);
        $return = $this->execCallbacks(__FUNCTION__, $return, $index, $context);

        if ($method = '_after_get_' . $index and method_exists($this, $method))
            $return = $this->$method($return, $index, $context);

        return $return;
    }
}