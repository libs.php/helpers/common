# Funciones comunes

## Instalación via composer

```bin
composer require jysperu/helpers-common
```

## Funciones `jysperu/function-empty-manager`

```php
/** Valida si el parametro enviado está vacío realmente */
function is_empty(mixed $val): bool

/** Obtener un valor por defecto en caso se detecte que el primer valor se encuentra vacío */
function def_empty(mixed $val, mixed ...$vals): mixed

/** Ejecutar una función si detecta que el valor no está vacío */
function non_empty(mixed $val, callable $cbk, mixed $def): mixed
```

## Funciones Debuggers

```php
/** Muestra los contenidos enviados en el HTML de manera formateada */
function print_array(mixed ...$array): void

/** Muestra los contenidos enviados en el HTML de manera formateada */
function print_r2(mixed ...$array): void

/** Muestra los contenidos enviados en el HTML de manera formateada y finaliza el proceso */
function die_array(mixed ...$array): void

/** Convierte un Array en un formato nestable para HTML */
function array2html(array $arr, int $lvl = 0): string
```

## Funciones uuid

```php
/** Generar un string uuid */
function uuid(): string
```

## Funciones with

```php
/** Permite interactuar con los parametros enviados */
function with(mixed ...$params): mixed
```

## Traits

```php
/** La clase asociada puede simular ser una variable array y a la vez poder ejecutar funciones de clase */
trait Arrayable {}

/**  La clase asociada puede alojar múltiples hooks que se pueden ejecutar en cualquier momento */
trait Callbackable {}

/** La clase asociada solo puede ser instanciada una sola vez */
trait Instanceable {}
```

## Clases

```php
class JArray extends ArrayObject
```
